lampstate	equ	0
timerstate	equ	1

org 00h 			; Assembly Starts from 0000H.
	sjmp main
org 0bh
	; timer0 interrupt
	mov p0, #0ffh
	clr tr0
	mov th0, #189
	mov tl0, #2
	reti

main:
; Main Program
	mov sp, #2fh
	mov p0, #0ffh
	mov p1, #0ffh
	mov p2, #0ffh
	mov p3, #0ffh

	mov r0, #0
	mov r1, #0
	mov r2, #0
	mov th0, #189
	mov tl0, #2
	mov tmod, #00h
	mov ie, #82h	; enable timer 0 interrupt
	mov 20h, r0 ; clear all bits

waitphase:
	mov a, p0
	xrl a, r0
	jb acc.1, phchanged
	sjmp waitphase

phchanged:
	jnb lampstate, nopulse
	mov p0, #0feh ; turn on the triac
	setb tr0
nopulse:
	mov r0, p0
	jnb p3.7, stop
	jb timerstate, countdown
	jb lampstate, waitphase ; when the lamp is on, other buttons do nothing
	jnb p3.5, start
	jnb p3.6, focus
	sjmp waitphase

stop:
	mov r1, #0
	mov r2, #0
	clr timerstate
	clr lampstate
	sjmp waitphase

countdown:
	djnz r1, waitphase
	djnz r2, waitphase
	sjmp stop

start:
	jnb p3.5, start ; wait for button release
	; get the positions of the 3 rotary switches
	clr p3.4
	acall getswitch
	setb p3.4
	mov a, #12 ; 10th of seconds at 60 cycles
	mul ab
	mov r1, a
	clr p3.3
	acall getswitch
	setb p3.3
	mov a, #120 ; seconds at 60 cycles
	mul ab
	add a, r1
	mov r1, a
	mov a, b
	addc a, #0
	mov r2, a
	clr p3.2
	acall getswitch
	setb p3.2
	mov a, #240 ; we do a trick here to multiply with 1200
	mul ab
	mov r3, a
	mov a, #5
	mul ab
	xch a, r3
	mov b, #4
	mul ab
	add a, r1
	mov r1, a
	mov a, r2
	addc a, b
	addc a, r3
	inc a ; add 1 because we use djnz to count down and check.
	mov r2, a
	; sanity check: Don't start, if all switches are set to zero
	cjne r1, #0, start_ok
	cjne r2, #1, start_ok
	dec r2
	sjmp waitphase
start_ok:
	setb timerstate
	setb lampstate
	sjmp waitphase

getswitch:
	mov b, #0
	jb p1.0, notzero
	ret
notzero:
	mov a, p1
	mov c, p3.0
	rrc a
	mov r3, #1
	mov c, p3.1
switchloop:
	jnb acc.0, found
	rrc a
	inc r3
	cjne r3, #10, switchloop
	; 10 can't be reached. There must be an open circuit
	; we break here to prevent wasting paper due to a defect.
	dec sp
	dec sp
	mov p3, #0ffh
	ajmp waitphase
found:
	mov b, r3
	ret

focus:
	setb lampstate
	ajmp waitphase
end  			; End Assembly
