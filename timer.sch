<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tPadExt" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bPadExt" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Anmerkung" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="fp2" color="10" fill="7" visible="no" active="no"/>
<layer number="103" name="fp3" color="11" fill="7" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="HC_TOP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="HC_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="13" fill="8" visible="no" active="no"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="14" fill="3" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="3" fill="3" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="values" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Frames">
<packages>
</packages>
<symbols>
<symbol name="RAHMEN_A3_Q">
<frame x1="0" y1="0" x2="381" y2="279.4" columns="15" rows="11" layer="94"/>
</symbol>
<symbol name="RAHMEN_FELD">
<wire x1="0.0508" y1="7.5819" x2="55.9308" y2="7.5819" width="0.254" layer="94"/>
<wire x1="55.9308" y1="15.2019" x2="0.0508" y2="15.2019" width="0.254" layer="94"/>
<wire x1="0.0508" y1="35.5219" x2="10.2108" y2="35.5219" width="0.254" layer="94"/>
<wire x1="10.2108" y1="35.5219" x2="33.0708" y2="35.5219" width="0.254" layer="94"/>
<wire x1="33.0708" y1="35.5219" x2="45.7708" y2="35.5219" width="0.254" layer="94"/>
<wire x1="45.7708" y1="35.5219" x2="55.9308" y2="35.5219" width="0.254" layer="94"/>
<wire x1="55.9308" y1="35.5219" x2="71.1708" y2="35.5219" width="0.254" layer="94"/>
<wire x1="71.1708" y1="35.5219" x2="86.4108" y2="35.5219" width="0.254" layer="94"/>
<wire x1="86.4108" y1="35.5219" x2="109.2708" y2="35.5219" width="0.254" layer="94"/>
<wire x1="109.2708" y1="40.6019" x2="55.9308" y2="40.6019" width="0.254" layer="94"/>
<wire x1="55.9308" y1="40.6019" x2="0.0508" y2="40.6019" width="0.254" layer="94"/>
<wire x1="109.2708" y1="40.6019" x2="109.2708" y2="35.5219" width="0.254" layer="94"/>
<wire x1="109.2708" y1="35.5219" x2="109.2708" y2="30.4419" width="0.254" layer="94"/>
<wire x1="109.2708" y1="30.4419" x2="109.2708" y2="25.3619" width="0.254" layer="94"/>
<wire x1="109.2708" y1="25.3619" x2="109.2708" y2="20.2819" width="0.254" layer="94"/>
<wire x1="109.2708" y1="20.2819" x2="109.2708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="109.2708" y1="30.4419" x2="71.1708" y2="30.4419" width="0.254" layer="94"/>
<wire x1="71.1708" y1="30.4419" x2="0.0508" y2="30.4419" width="0.254" layer="94"/>
<wire x1="55.9308" y1="-0.0381" x2="55.9308" y2="3.7719" width="0.254" layer="94"/>
<wire x1="55.9308" y1="3.7719" x2="55.9308" y2="7.5819" width="0.254" layer="94"/>
<wire x1="55.9308" y1="7.5819" x2="55.9308" y2="15.2019" width="0.254" layer="94"/>
<wire x1="55.9308" y1="15.2019" x2="55.9308" y2="25.3619" width="0.254" layer="94"/>
<wire x1="10.2108" y1="35.5219" x2="10.2108" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="33.0708" y1="35.5219" x2="33.0708" y2="19.0119" width="0.254" layer="94"/>
<wire x1="33.0708" y1="19.0119" x2="33.0708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="45.7708" y1="35.5219" x2="45.7708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="55.9308" y1="25.3619" x2="55.9308" y2="35.5219" width="0.254" layer="94"/>
<wire x1="55.9308" y1="15.2019" x2="71.1708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="71.1708" y1="15.2019" x2="86.4108" y2="15.2019" width="0.254" layer="94"/>
<wire x1="86.4108" y1="15.2019" x2="109.2708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="55.9308" y1="25.3619" x2="109.2708" y2="25.3619" width="0.254" layer="94"/>
<wire x1="55.9308" y1="35.5219" x2="55.9308" y2="40.6019" width="0.254" layer="94"/>
<wire x1="71.1708" y1="30.4419" x2="71.1708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="71.1708" y1="30.4419" x2="71.1708" y2="35.5219" width="0.254" layer="94"/>
<wire x1="86.4108" y1="35.5219" x2="86.4108" y2="15.2019" width="0.254" layer="94"/>
<wire x1="109.2708" y1="40.6019" x2="167.6908" y2="40.6019" width="0.254" layer="94"/>
<wire x1="109.2708" y1="35.5219" x2="167.6908" y2="35.5219" width="0.254" layer="94"/>
<wire x1="0.0508" y1="3.7719" x2="55.9308" y2="3.7719" width="0.254" layer="94"/>
<wire x1="109.2708" y1="15.2019" x2="109.2708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="109.2708" y1="20.2819" x2="56.134" y2="20.32" width="0.254" layer="94"/>
<wire x1="55.88" y1="19.0119" x2="33.0708" y2="19.0119" width="0.254" layer="94"/>
<wire x1="33.0708" y1="19.0119" x2="0.0508" y2="19.0119" width="0.254" layer="94"/>
<wire x1="109.2708" y1="15.2019" x2="115.6208" y2="15.2019" width="0.254" layer="94"/>
<wire x1="115.6208" y1="15.2019" x2="121.9708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="121.9708" y1="15.2019" x2="128.3208" y2="15.2019" width="0.254" layer="94"/>
<wire x1="128.3208" y1="15.2019" x2="134.6708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="134.6708" y1="15.2019" x2="141.0208" y2="15.2019" width="0.254" layer="94"/>
<wire x1="141.0208" y1="15.2019" x2="147.3708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="147.3708" y1="15.2019" x2="153.7208" y2="15.2019" width="0.254" layer="94"/>
<wire x1="153.7208" y1="15.2019" x2="160.0708" y2="15.2019" width="0.254" layer="94"/>
<wire x1="160.0708" y1="15.2019" x2="167.6908" y2="15.2019" width="0.254" layer="94"/>
<wire x1="115.6208" y1="15.2019" x2="115.6208" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="121.9708" y1="15.2019" x2="121.9708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="128.3208" y1="15.2019" x2="128.3208" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="134.6708" y1="15.2019" x2="134.6708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="141.0208" y1="15.2019" x2="141.0208" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="147.3708" y1="15.2019" x2="147.3708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="153.7208" y1="15.2019" x2="153.7208" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="160.0708" y1="15.2019" x2="160.0708" y2="-0.0381" width="0.254" layer="94"/>
<wire x1="0" y1="11.43" x2="55.88" y2="11.43" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="55.88" y2="22.86" width="0.254" layer="94"/>
<wire x1="55.88" y1="26.67" x2="0" y2="26.67" width="0.254" layer="94"/>
<wire x1="0" y1="40.5638" x2="0" y2="0" width="0.254" layer="94"/>
<text x="48.3108" y="32.3469" size="1.27" layer="95">Name</text>
<text x="88.0618" y="32.3469" size="1.27" layer="95">Name</text>
<text x="1.3208" y="32.3469" size="1.27" layer="95">Rev.Nr.</text>
<text x="11.4808" y="32.3469" size="1.27" layer="95">Änderung./modific.</text>
<text x="34.3408" y="32.3469" size="1.27" layer="95">Date</text>
<text x="57.4548" y="27.1399" size="1.27" layer="95">Bearb./subscr.</text>
<text x="73.7108" y="32.2199" size="1.27" layer="95">Datum</text>
<text x="57.4548" y="22.0599" size="1.27" layer="95">Gepr./chk'd</text>
<text x="161.3408" y="12.6619" size="1.27" layer="94">Blatt</text>
<text x="161.3408" y="10.1219" size="1.27" layer="94">page</text>
<text x="68.6308" y="5.0419" size="5.08" layer="95">Draconitron</text>
<text x="161.544" y="2.54" size="2.54" layer="94" ratio="15">&gt;SHEET</text>
<text x="133.096" y="37.084" size="2.1844" layer="94" ratio="15">&gt;DRAWING_NAME</text>
<text x="110.49" y="37.084" size="1.9304" layer="94" ratio="15">Zeichnungsname:</text>
<text x="57.912" y="4.318" size="8.89" layer="95"></text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RAHMEN3Q" prefix="FRAME">
<gates>
<gate name="G$1" symbol="RAHMEN_A3_Q" x="0" y="0"/>
<gate name="G$2" symbol="RAHMEN_FELD" x="209.55" y="3.81"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Microcontroller">
<packages>
<package name="DIL40">
<description>&lt;B&gt;Dual In Line&lt;/B&gt;</description>
<wire x1="-26.416" y1="-1.27" x2="-26.416" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-26.416" y1="1.27" x2="-26.416" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="26.416" y1="-6.604" x2="26.416" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-26.416" y1="6.604" x2="-26.416" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-26.416" y1="6.604" x2="26.416" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-26.416" y1="-6.604" x2="26.416" y2="-6.604" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-21.59" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-19.05" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-16.51" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="16.51" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="19.05" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="21.59" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="24.13" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="24.13" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="21.59" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="19.05" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="16.51" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="29" x="3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="30" x="1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="31" x="-1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="32" x="-3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="33" x="-6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="34" x="-8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="35" x="-11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="36" x="-13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="37" x="-16.51" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="38" x="-19.05" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="39" x="-21.59" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="40" x="-24.13" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-26.67" y="-6.35" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-21.59" y="-2.2352" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="AT89S51">
<wire x1="-15.24" y1="-33.02" x2="15.24" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="15.24" y1="-33.02" x2="15.24" y2="33.02" width="0.4064" layer="94"/>
<wire x1="15.24" y1="33.02" x2="-15.24" y2="33.02" width="0.4064" layer="94"/>
<wire x1="-15.24" y1="33.02" x2="-15.24" y2="-33.02" width="0.4064" layer="94"/>
<text x="-15.24" y="-35.56" size="1.778" layer="96">&gt;VALUE</text>
<text x="-15.24" y="33.655" size="1.778" layer="95">&gt;NAME</text>
<pin name="!EA!/VPP" x="20.32" y="-20.32" length="middle" direction="in" rot="R180"/>
<pin name="P0.0_AD0" x="20.32" y="25.4" length="middle" rot="R180"/>
<pin name="P0.1_AD1" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="P0.2_AD2" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="P0.3_AD3" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="P0.4_AD4" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="P0.5_AD5" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="P0.6_AD6" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="P0.7_AD7" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="XTAL1" x="-20.32" y="25.4" length="middle" direction="in"/>
<pin name="XTAL2" x="-20.32" y="17.78" length="middle" direction="in"/>
<pin name="RST" x="-20.32" y="30.48" length="middle" direction="in"/>
<pin name="ALE/!PROG" x="20.32" y="-22.86" length="middle" direction="out" rot="R180"/>
<pin name="!PSEN" x="20.32" y="-25.4" length="middle" direction="out" rot="R180"/>
<pin name="P2.0_A8" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="P2.1_A9" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="P2.2_A10" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="P2.3_A11" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="P2.4_A12" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="P2.5_A13" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="P2.6_A14" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="P2.7_A15" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="P1.0" x="-20.32" y="12.7" length="middle"/>
<pin name="P1.1" x="-20.32" y="10.16" length="middle"/>
<pin name="P1.2" x="-20.32" y="7.62" length="middle"/>
<pin name="P1.3" x="-20.32" y="5.08" length="middle"/>
<pin name="P1.4" x="-20.32" y="2.54" length="middle"/>
<pin name="P1.5_MOSI" x="-20.32" y="0" length="middle"/>
<pin name="P1.6_MISO" x="-20.32" y="-2.54" length="middle"/>
<pin name="P1.7_SCK" x="-20.32" y="-5.08" length="middle"/>
<pin name="P3.0/RXD" x="-20.32" y="-10.16" length="middle"/>
<pin name="P3.1/TXD" x="-20.32" y="-12.7" length="middle"/>
<pin name="P3.2/!INT0" x="-20.32" y="-15.24" length="middle"/>
<pin name="P3.3/!INT1" x="-20.32" y="-17.78" length="middle"/>
<pin name="P3.4/T0" x="-20.32" y="-20.32" length="middle"/>
<pin name="P3.5/T1" x="-20.32" y="-22.86" length="middle"/>
<pin name="P3.6/!WR" x="-20.32" y="-25.4" length="middle"/>
<pin name="P3.7/!RD" x="-20.32" y="-27.94" length="middle"/>
<pin name="VCC" x="20.32" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="20.32" y="-30.48" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AT89S51-*" prefix="IC">
<description>&lt;b&gt;8-bit Microcontroller with 4K BytesIn-System Programmable Flash&lt;/b&gt;&lt;p&gt;
Source: http://www.atmel.com/dyn/resources/prod_documents/doc2487.pdf</description>
<gates>
<gate name="P" symbol="AT89S51" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="DIL40">
<connects>
<connect gate="P" pin="!EA!/VPP" pad="31"/>
<connect gate="P" pin="!PSEN" pad="29"/>
<connect gate="P" pin="ALE/!PROG" pad="30"/>
<connect gate="P" pin="GND" pad="20"/>
<connect gate="P" pin="P0.0_AD0" pad="39"/>
<connect gate="P" pin="P0.1_AD1" pad="38"/>
<connect gate="P" pin="P0.2_AD2" pad="37"/>
<connect gate="P" pin="P0.3_AD3" pad="36"/>
<connect gate="P" pin="P0.4_AD4" pad="35"/>
<connect gate="P" pin="P0.5_AD5" pad="34"/>
<connect gate="P" pin="P0.6_AD6" pad="33"/>
<connect gate="P" pin="P0.7_AD7" pad="32"/>
<connect gate="P" pin="P1.0" pad="1"/>
<connect gate="P" pin="P1.1" pad="2"/>
<connect gate="P" pin="P1.2" pad="3"/>
<connect gate="P" pin="P1.3" pad="4"/>
<connect gate="P" pin="P1.4" pad="5"/>
<connect gate="P" pin="P1.5_MOSI" pad="6"/>
<connect gate="P" pin="P1.6_MISO" pad="7"/>
<connect gate="P" pin="P1.7_SCK" pad="8"/>
<connect gate="P" pin="P2.0_A8" pad="21"/>
<connect gate="P" pin="P2.1_A9" pad="22"/>
<connect gate="P" pin="P2.2_A10" pad="23"/>
<connect gate="P" pin="P2.3_A11" pad="24"/>
<connect gate="P" pin="P2.4_A12" pad="25"/>
<connect gate="P" pin="P2.5_A13" pad="26"/>
<connect gate="P" pin="P2.6_A14" pad="27"/>
<connect gate="P" pin="P2.7_A15" pad="28"/>
<connect gate="P" pin="P3.0/RXD" pad="10"/>
<connect gate="P" pin="P3.1/TXD" pad="11"/>
<connect gate="P" pin="P3.2/!INT0" pad="12"/>
<connect gate="P" pin="P3.3/!INT1" pad="13"/>
<connect gate="P" pin="P3.4/T0" pad="14"/>
<connect gate="P" pin="P3.5/T1" pad="15"/>
<connect gate="P" pin="P3.6/!WR" pad="16"/>
<connect gate="P" pin="P3.7/!RD" pad="17"/>
<connect gate="P" pin="RST" pad="9"/>
<connect gate="P" pin="VCC" pad="40"/>
<connect gate="P" pin="XTAL1" pad="19"/>
<connect gate="P" pin="XTAL2" pad="18"/>
</connects>
<technologies>
<technology name="">
<attribute name="REICHEL" value="AT 89S51 PDIP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="R-0805">
<packages>
<package name="0805">
<description>SMD 0805 nach IPC Surface Mount Design and Land Pattern Standard IPC-SM-782
(Keepout: 150um Abstand vom Kupfer nach Becker &amp; Müller Designrules)</description>
<text x="-1.5" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.135" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.075" y1="0.7" x2="-0.2775" y2="0.7" width="0.05" layer="51"/>
<wire x1="-0.1584375" y1="0.7" x2="0.1584375" y2="0.7" width="0.05" layer="21"/>
<wire x1="0.2775" y1="0.7" x2="1.075" y2="0.7" width="0.05" layer="51"/>
<wire x1="-1.075" y1="-0.7" x2="-0.2775" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-0.1584375" y1="-0.7" x2="0.1584375" y2="-0.7" width="0.05" layer="21"/>
<wire x1="0.2775" y1="-0.7" x2="1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-1.075" y1="0.7" x2="-1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-0.2775" y1="0.7" x2="-0.2775" y2="-0.7" width="0.05" layer="51"/>
<wire x1="1.075" y1="0.7" x2="1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="0.2775" y1="0.7" x2="0.2775" y2="-0.7" width="0.05" layer="51"/>
<rectangle x1="-1.075" y1="-0.7" x2="-0.275" y2="0.7" layer="51"/>
<rectangle x1="0.275" y1="-0.7" x2="1.075" y2="0.7" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<rectangle x1="-1.75" y1="-0.9" x2="-0.15" y2="0.9" layer="39"/>
<rectangle x1="0.15" y1="-0.9" x2="1.75" y2="0.9" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10K" prefix="R">
<description>1%</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5K6" prefix="R">
<description>1%</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="220R" prefix="R">
<description>1%</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Crystal">
<packages>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="21" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
</package>
<package name="HC49U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-3.175" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="9.271" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<text x="-5.461" y="-1.397" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-4.445" x2="6.35" y2="-1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.905" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-LM">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.414" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-5.08" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
</packages>
<symbols>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="7.3728MHZ" prefix="XTAL">
<description>Standardquarz</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="/S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/H" package="HC49U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/LM" package="HC49U-LM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C-0805">
<packages>
<package name="0805">
<description>SMD 0805 nach IPC Surface Mount Design and Land Pattern Standard IPC-SM-782
(Keepout: 150um Abstand vom Kupfer nach Becker &amp; Müller Designrules)</description>
<text x="-1.5" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.135" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.075" y1="0.7" x2="-0.2775" y2="0.7" width="0.05" layer="51"/>
<wire x1="-0.1584375" y1="0.7" x2="0.1584375" y2="0.7" width="0.05" layer="21"/>
<wire x1="0.2775" y1="0.7" x2="1.075" y2="0.7" width="0.05" layer="51"/>
<wire x1="-1.075" y1="-0.7" x2="-0.2775" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-0.1584375" y1="-0.7" x2="0.1584375" y2="-0.7" width="0.05" layer="21"/>
<wire x1="0.2775" y1="-0.7" x2="1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-1.075" y1="0.7" x2="-1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="-0.2775" y1="0.7" x2="-0.2775" y2="-0.7" width="0.05" layer="51"/>
<wire x1="1.075" y1="0.7" x2="1.075" y2="-0.7" width="0.05" layer="51"/>
<wire x1="0.2775" y1="0.7" x2="0.2775" y2="-0.7" width="0.05" layer="51"/>
<rectangle x1="-1.075" y1="-0.7" x2="-0.275" y2="0.7" layer="51"/>
<rectangle x1="0.275" y1="-0.7" x2="1.075" y2="0.7" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<rectangle x1="-1.75" y1="-0.9" x2="-0.15" y2="0.9" layer="39"/>
<rectangle x1="0.15" y1="-0.9" x2="1.75" y2="0.9" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22P" prefix="C">
<description>Keramik, 63V, 5%, NPO</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="100N" prefix="C">
<description>Keramik, 50V, 10%, X7R</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="0V">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.4064" layer="94"/>
<text x="-1.143" y="-2.032" size="1.778" layer="96">&gt;VALUE</text>
<pin name="0V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="R">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="R" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="1.27" size="1.778" layer="94" align="center">~</text>
</symbol>
<symbol name="PE">
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-4.445" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PE" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+05V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="-05V">
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="94"/>
<circle x="0" y="-1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<pin name="-5V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="0V" symbol="0V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PE" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="PE" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="-5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="-5V" symbol="-05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C-Draht">
<packages>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.127" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.127" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.127" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.127" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.127" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.127" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.127" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.127" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-3.429" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="220N/305V/MKP-X2" prefix="C">
<description>MKP-X2, 10%, RM15</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="REICHELT" value="MKP-X2 220N" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="R-th">
<packages>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="220R" prefix="R">
<description>Metallschicht</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="470K" prefix="R">
<description>Metallschicht</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode">
<packages>
<package name="DO35-7">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
diameter 2 mm, horizontal, grid 7.62 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-0.635" y1="0" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.286" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="0.762" x2="2.286" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="2.286" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.286" y1="0.762" x2="-2.032" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.286" y1="-0.762" x2="-2.032" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.032" y1="-1.016" x2="2.032" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0.762" x2="-2.286" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.1524" layer="21"/>
<pad name="C" x="-3.81" y="0" drill="0.8128" shape="long"/>
<pad name="A" x="3.81" y="0" drill="0.8128" shape="long"/>
<text x="-2.286" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.905" y1="-1.016" x2="-1.397" y2="1.016" layer="21"/>
<rectangle x1="2.286" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.286" y2="0.254" layer="21"/>
</package>
<package name="DO07">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
diameter 2.54 mm, horizontal, grid 10.16 mm</description>
<wire x1="5.08" y1="0" x2="4.191" y2="0" width="0.508" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.191" y2="0" width="0.508" layer="51"/>
<wire x1="-0.635" y1="0" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.27" x2="3.556" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.556" y1="1.016" x2="-3.302" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.556" y1="-1.016" x2="-3.302" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="3.302" y1="-1.27" x2="3.556" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="3.556" y1="-1.016" x2="3.556" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.016" x2="-3.556" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.27" x2="3.302" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="3.302" y2="-1.27" width="0.1524" layer="21"/>
<pad name="C" x="-5.08" y="0" drill="0.8128" shape="long"/>
<pad name="A" x="5.08" y="0" drill="0.8128" shape="long"/>
<text x="-3.429" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="1.27" layer="21"/>
<rectangle x1="3.556" y1="-0.254" x2="4.191" y2="0.254" layer="51"/>
<rectangle x1="-4.191" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
</package>
<package name="MINIMELF">
<description>&lt;b&gt;Mini Melf Diode&lt;/b&gt;</description>
<wire x1="1.3208" y1="0.7874" x2="-1.3208" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.7874" x2="-1.3208" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="0.5" y1="0.5" x2="-0.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="0.5" y2="0.5" width="0.2032" layer="21"/>
<smd name="C" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8636" x2="-1.2954" y2="0.8636" layer="51"/>
<rectangle x1="1.2954" y1="-0.8636" x2="1.8542" y2="0.8636" layer="51"/>
<rectangle x1="-0.8636" y1="-0.7874" x2="-0.254" y2="0.7874" layer="21"/>
</package>
<package name="DO35-10">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
diameter 2 mm, horizontal, grid 10.16 mm</description>
<wire x1="5.08" y1="0" x2="4.191" y2="0" width="0.508" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.191" y2="0" width="0.508" layer="51"/>
<wire x1="-0.635" y1="0" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.286" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.286" y1="0.762" x2="-2.032" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.286" y1="-0.762" x2="-2.032" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.032" y1="-1.016" x2="2.286" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-0.762" x2="2.286" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0.762" x2="-2.286" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="2.032" y2="-1.016" width="0.1524" layer="21"/>
<pad name="C" x="-5.08" y="0" drill="0.8128" shape="long"/>
<pad name="A" x="5.08" y="0" drill="0.8128" shape="long"/>
<text x="-2.159" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.905" y1="-1.016" x2="-1.397" y2="1.016" layer="21"/>
<rectangle x1="2.286" y1="-0.254" x2="4.191" y2="0.254" layer="21"/>
<rectangle x1="-4.191" y1="-0.254" x2="-2.286" y2="0.254" layer="21"/>
</package>
<package name="D-2.5">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<wire x1="0.508" y1="0.762" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="C" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ZD">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-1.778" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.778" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BZX55C5V1" prefix="D">
<description>Zener, 500mW</description>
<gates>
<gate name="U" symbol="ZD" x="0" y="0"/>
</gates>
<devices>
<device name="/10" package="DO35-10">
<connects>
<connect gate="U" pin="A" pad="A"/>
<connect gate="U" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="DO35-7">
<connects>
<connect gate="U" pin="A" pad="A"/>
<connect gate="U" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/V" package="D-2.5">
<connects>
<connect gate="U" pin="A" pad="A"/>
<connect gate="U" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1N4007" prefix="D">
<description>1000 V, 1 A</description>
<gates>
<gate name="U" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO07">
<connects>
<connect gate="U" pin="A" pad="A"/>
<connect gate="U" pin="K" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="REICHELT" value="1N 4007" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LL4148" prefix="D">
<description>General purpose diode, 100V, 150mA</description>
<gates>
<gate name="U" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MINIMELF">
<connects>
<connect gate="U" pin="A" pad="A"/>
<connect gate="U" pin="K" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="REICHELT" value="LL4148" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C-Elko">
<packages>
<package name="E7,5-16">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.62 mm, diameter 16 mm</description>
<wire x1="0.635" y1="0" x2="2.032" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-2.032" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.905" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.905" x2="-0.254" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.905" x2="-0.254" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.905" x2="-0.889" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.493" y1="0" x2="-5.969" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-0.762" x2="-6.731" y2="0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="8.255" width="0.1524" layer="21"/>
<pad name="-" x="3.81" y="0" drill="1.016" diameter="3.1496" shape="octagon"/>
<pad name="+" x="-3.81" y="0" drill="1.016" diameter="3.1496"/>
<text x="7.874" y="3.81" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.2926" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.905" x2="0.889" y2="1.905" layer="21"/>
</package>
<package name="E2,5-5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 5 mm</description>
<wire x1="-1.651" y1="1.27" x2="-1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="-1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.27" x2="-1.143" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.27" x2="-1.397" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<text x="2.413" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.413" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CPOL">
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<text x="1.143" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-4.5974" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="470U/63V" prefix="C">
<description>Elko 470 µF / 63V / 16x25 / RM7.5 / low ESR</description>
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="E7,5-16">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10U/63V" prefix="C">
<description>Elko 10 µF / 63V / 5mm / RM2.5 / Panasonic</description>
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="E2,5-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="REICHELT" value="EB-A 10U 63" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Transistor">
<packages>
<package name="TO92">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-2.6549" y2="-0.254" width="0.127" layer="21" curve="-32.781"/>
<wire x1="-2.6549" y1="-0.254" x2="-0.7863" y2="2.5485" width="0.127" layer="21" curve="-78.3185"/>
<wire x1="0.7863" y1="2.5484" x2="2.0945" y2="-1.651" width="0.127" layer="21" curve="-111.1"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.2537" y1="-0.254" x2="-0.2863" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.2537" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.2863" y1="-0.254" x2="0.2863" y2="-0.254" width="0.127" layer="21"/>
<wire x1="2.2537" y1="-0.254" x2="2.6549" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.2863" y1="-0.254" x2="2.2537" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.7863" y1="2.5485" x2="0.7863" y2="2.5485" width="0.127" layer="51" curve="-34.2936"/>
<pad name="1" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="0" y="1.905" drill="0.8128" shape="octagon"/>
<pad name="3" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.635" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="0.635" size="1.27" layer="51" ratio="10">2</text>
<text x="-2.159" y="0" size="1.27" layer="51" ratio="10">3</text>
<text x="1.143" y="0" size="1.27" layer="51" ratio="10">1</text>
</package>
<package name="TO220AB">
<description>&lt;B&gt;TO220 AB&lt;/B&gt;&lt;p&gt;
3-lead molded horzintal</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.1524" layer="21"/>
<circle x="0" y="11.176" radius="2.921" width="0" layer="42"/>
<circle x="0" y="11.176" radius="2.921" width="0" layer="43"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.1" diameter="1.5" shape="long" rot="R90"/>
<pad name="2" x="0" y="-6.35" drill="1.1" diameter="1.5" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-6.35" drill="1.1" diameter="1.5" shape="long" rot="R90"/>
<text x="-5.461" y="-1.27" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.937" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="7.874" size="0.9906" layer="21" ratio="10">A17,5mm</text>
<rectangle x1="2.159" y1="-4.445" x2="2.921" y2="-3.81" layer="21"/>
<rectangle x1="-0.381" y1="-4.445" x2="0.381" y2="-3.81" layer="21"/>
<rectangle x1="-2.921" y1="-4.445" x2="-2.159" y2="-3.81" layer="21"/>
<rectangle x1="-3.175" y1="-3.81" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-3.81" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-3.81" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="2.159" y1="-6.35" x2="2.921" y2="-4.445" layer="51"/>
<rectangle x1="-0.381" y1="-6.35" x2="0.381" y2="-4.445" layer="51"/>
<rectangle x1="-2.921" y1="-6.35" x2="-2.159" y2="-4.445" layer="51"/>
<hole x="0" y="11.176" drill="3.302"/>
</package>
</packages>
<symbols>
<symbol name="PNP">
<wire x1="2.086" y1="1.678" x2="1.578" y2="2.594" width="0.1524" layer="94"/>
<wire x1="1.578" y1="2.594" x2="0.516" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.516" y1="1.478" x2="2.086" y2="1.678" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.124" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="TRIAC">
<wire x1="-2.54" y1="2.794" x2="-1.016" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="0.254" y2="2.794" width="0.254" layer="94"/>
<wire x1="0.254" y1="2.794" x2="-2.54" y2="2.794" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.794" x2="0.254" y2="2.794" width="0.254" layer="94"/>
<wire x1="-0.254" y1="0.508" x2="1.016" y2="2.794" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.794" x2="2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="-0.254" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.794" x2="2.54" y2="2.794" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.508" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="-1.905" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.508" x2="-2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="-0.254" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="0" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A2" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="A1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2N3906" prefix="Q">
<description>PNP Transistor</description>
<gates>
<gate name="U" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO92">
<connects>
<connect gate="U" pin="B" pad="2"/>
<connect gate="U" pin="C" pad="1"/>
<connect gate="U" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BT136" prefix="T">
<description>&lt;b&gt;Triac BT136 series E sensitive gate&lt;/b&gt;&lt;p&gt;
Source: http://www.nxp.com/acrobat_download/datasheets/BT136_SERIES_E_2.pdf</description>
<gates>
<gate name="-1" symbol="TRIAC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220AB">
<connects>
<connect gate="-1" pin="A1" pad="1"/>
<connect gate="-1" pin="A2" pad="2"/>
<connect gate="-1" pin="G" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2N3904" prefix="Q">
<description>NPN TRANSISTOR</description>
<gates>
<gate name="U" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO92">
<connects>
<connect gate="U" pin="B" pad="2"/>
<connect gate="U" pin="C" pad="1"/>
<connect gate="U" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch">
<description>&lt;b&gt;Switches&lt;/b&gt;&lt;p&gt;
Marquardt, Siemens, C&amp;K, ITT, and others&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="24-1POLE36">
<description>&lt;b&gt;SERIES 24&lt;/b&gt; Rotary Switches one pole 36°&lt;p&gt;
Source: www.grayhill.com .. J-28.pdf</description>
<wire x1="-0.1558" y1="3.0179" x2="-2.9331" y2="-0.8046" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="11.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.075" width="0.2032" layer="21"/>
<pad name="10" x="9.22" y="0" drill="1.4" diameter="2.1844"/>
<pad name="6" x="-7.47" y="5.41" drill="1.4" diameter="2.1844" rot="R180"/>
<pad name="7" x="-2.87" y="8.81" drill="1.4" diameter="2.1844" rot="R180"/>
<pad name="5" x="-9.22" y="0" drill="1.4" diameter="2.1844" rot="R180"/>
<pad name="1" x="7.47" y="-5.41" drill="1.4" diameter="2.1844"/>
<pad name="2" x="2.87" y="-8.81" drill="1.4" diameter="2.1844"/>
<pad name="9" x="7.47" y="5.41" drill="1.4" diameter="2.1844"/>
<pad name="8" x="2.87" y="8.81" drill="1.4" diameter="2.1844"/>
<pad name="4" x="-7.47" y="-5.41" drill="1.4" diameter="2.1844" rot="R180"/>
<pad name="3" x="-2.87" y="-8.81" drill="1.4" diameter="2.1844" rot="R180"/>
<pad name="C" x="13.34" y="0" drill="1.4" diameter="2.1844"/>
<text x="-3.81" y="12.065" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="11.825" y1="-0.575" x2="13.575" y2="0.55" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="DS-110">
<wire x1="-16.51" y1="1.905" x2="-15.24" y2="1.905" width="0.254" layer="94"/>
<wire x1="-16.51" y1="1.905" x2="-16.51" y2="0" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-1.905" x2="-17.78" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-16.51" y1="0" x2="-14.605" y2="0" width="0.1524" layer="94"/>
<wire x1="-16.51" y1="0" x2="-16.51" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-13.97" y1="0" x2="-13.462" y2="0" width="0.1524" layer="94"/>
<wire x1="-12.446" y1="0" x2="-12.065" y2="0" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="0" x2="-10.287" y2="0" width="0.1524" layer="94"/>
<wire x1="-13.462" y1="0" x2="-12.954" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-12.954" y1="-0.762" x2="-12.446" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="3.175" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-3.175" x2="-10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-10.16" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.175" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="10.16" y1="3.175" x2="10.16" y2="5.08" width="0.1524" layer="94"/>
<wire x1="12.7" y1="3.175" x2="12.7" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="10.16" y2="5.08" width="0.1524" layer="94"/>
<wire x1="10.16" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="12.7" y2="-3.175" width="0.1524" layer="94"/>
<circle x="-5.08" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="-2.54" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="2.54" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="-10.16" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="-7.62" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="-10.16" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-7.62" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-5.08" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-10.16" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-7.62" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-5.08" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="5.08" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="5.08" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="7.62" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="10.16" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="12.7" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="7.62" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="10.16" y="5.08" radius="0.254" width="0.254" layer="94"/>
<circle x="7.62" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="10.16" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="12.7" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<text x="-16.51" y="3.175" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-13.97" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-10.541" y="-1.397" size="1.27" layer="94" rot="R90">1</text>
<text x="-6.985" y="-0.635" size="1.27" layer="94" rot="R90">2</text>
<text x="-4.445" y="-0.635" size="1.27" layer="94" rot="R90">3</text>
<text x="-1.905" y="-0.635" size="1.27" layer="94" rot="R90">4</text>
<text x="0.635" y="-0.635" size="1.27" layer="94" rot="R90">5</text>
<text x="3.175" y="-0.635" size="1.27" layer="94" rot="R90">6</text>
<text x="5.715" y="-0.635" size="1.27" layer="94" rot="R90">7</text>
<text x="8.255" y="-0.635" size="1.27" layer="94" rot="R90">8</text>
<text x="10.795" y="-0.635" size="1.27" layer="94" rot="R90">9</text>
<text x="13.335" y="-1.016" size="1.27" layer="94" rot="R90">10</text>
<pin name="1" x="-10.16" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="S" x="-10.16" y="7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="-7.62" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="3" x="-5.08" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="4" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="6" x="2.54" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="7" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="8" x="7.62" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="9" x="10.16" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="10" x="12.7" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="24-1POLE36" prefix="S">
<description>&lt;b&gt;SERIES 24&lt;/b&gt; Rotary Switches one pole 36°&lt;p&gt;
Source: www.grayhill.com .. J-28.pdf</description>
<gates>
<gate name="G$1" symbol="DS-110" x="0" y="0"/>
</gates>
<devices>
<device name="" package="24-1POLE36">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="S" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schalter">
<description>&lt;b&gt;Schalter für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="EINTASTER">
<description>Dummy</description>
<pad name="14" x="0" y="-1.905" drill="0.8" shape="square"/>
<pad name="13" x="0" y="1.905" drill="0.8" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TASTER_SCHLIESSER">
<wire x1="-0.762" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.143" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="-3.302" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.143" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-4.191" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.937" y1="1.143" x2="-5.08" y2="1.143" width="0.1524" layer="94"/>
<wire x1="-3.937" y1="-1.143" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.651" width="0.254" layer="94"/>
<text x="-3.81" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-3.81" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="14" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="13" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TASTER_SCHLIESSER" prefix="S" uservalue="yes">
<description>Ein-Taster (Arbeitskontakt)</description>
<gates>
<gate name="G$1" symbol="TASTER_SCHLIESSER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EINTASTER">
<connects>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Fuses">
<packages>
<package name="SH22,5_SMD">
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="5.08" y2="2.921" width="0.0508" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-10.795" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-11.43" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-3.683" x2="-11.43" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-2.032" x2="-11.176" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-3.175" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.175" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-2.032" x2="-11.43" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-1.27" x2="-11.176" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-1.27" x2="-11.176" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-3.683" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-3.683" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="10.795" y2="3.683" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="11.43" y2="3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.683" x2="11.43" y2="2.032" width="0.1524" layer="21"/>
<wire x1="11.176" y1="2.032" x2="11.176" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.175" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.175" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="2.032" x2="11.43" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.43" y1="1.27" x2="11.176" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.176" y1="1.27" x2="11.176" y2="2.032" width="0.1524" layer="51"/>
<wire x1="12.573" y1="3.937" x2="12.573" y2="1.651" width="0.1524" layer="21"/>
<wire x1="12.573" y1="-1.651" x2="12.573" y2="1.651" width="0.1524" layer="51"/>
<wire x1="12.573" y1="-1.651" x2="12.573" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="3.937" x2="-12.573" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="-1.651" x2="-12.573" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-12.573" y1="-1.651" x2="-12.573" y2="-3.937" width="0.1524" layer="21"/>
<smd name="1" x="-8.5" y="0" dx="6" dy="6" layer="1"/>
<smd name="2" x="8.5" y="0" dx="6" dy="6" layer="1"/>
<text x="-12.7" y="-7.366" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="2.413" y="-7.366" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.541" y1="-2.032" x2="-10.16" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="1.905" x2="-9.652" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="-2.032" x2="-9.652" y2="-1.905" layer="51"/>
<rectangle x1="-5.715" y1="-3.175" x2="-5.08" y2="-2.032" layer="21"/>
<rectangle x1="-5.715" y1="2.032" x2="-5.08" y2="3.175" layer="21"/>
<rectangle x1="-5.715" y1="-2.032" x2="-5.08" y2="2.032" layer="21"/>
<rectangle x1="10.16" y1="-2.032" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="9.652" y1="-2.032" x2="10.541" y2="-1.905" layer="51"/>
<rectangle x1="9.652" y1="1.905" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="5.08" y1="2.032" x2="5.715" y2="3.175" layer="21"/>
<rectangle x1="5.08" y1="-3.175" x2="5.715" y2="-2.032" layer="21"/>
<rectangle x1="5.08" y1="-2.032" x2="5.715" y2="2.032" layer="21"/>
<rectangle x1="-5.08" y1="2.159" x2="5.08" y2="2.667" layer="21"/>
<rectangle x1="-5.08" y1="-2.667" x2="5.08" y2="-2.159" layer="21"/>
<rectangle x1="5.715" y1="-2.032" x2="9.652" y2="-1.905" layer="21"/>
<rectangle x1="5.715" y1="1.905" x2="9.652" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="1.905" x2="-5.715" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="-2.032" x2="-5.715" y2="-1.905" layer="21"/>
<rectangle x1="-10.541" y1="-2.54" x2="-5.715" y2="-2.032" layer="21"/>
<rectangle x1="-10.541" y1="2.032" x2="-5.715" y2="2.54" layer="21"/>
<rectangle x1="5.715" y1="2.032" x2="10.541" y2="2.54" layer="21"/>
<rectangle x1="5.715" y1="-2.54" x2="10.541" y2="-2.032" layer="21"/>
<wire x1="-12.573" y1="-5.461" x2="12.573" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="12.573" y1="5.461" x2="-12.573" y2="5.461" width="0.1524" layer="21"/>
<wire x1="12.573" y1="5.461" x2="12.573" y2="1.651" width="0.1524" layer="21"/>
<wire x1="12.573" y1="-1.651" x2="12.573" y2="1.651" width="0.1524" layer="51"/>
<wire x1="12.573" y1="-1.651" x2="12.573" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="5.461" x2="-12.573" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="-1.651" x2="-12.573" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-12.573" y1="-1.651" x2="-12.573" y2="-5.461" width="0.1524" layer="21"/>
</package>
<package name="SH22,5A">
<description>&lt;b&gt;FUSE HOLDER&lt;/b&gt;&lt;p&gt; grid 22,5 mm, isolated cap&lt;p&gt;</description>
<wire x1="-12.573" y1="-5.461" x2="12.573" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="12.573" y1="5.461" x2="-12.573" y2="5.461" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="5.08" y2="2.921" width="0.0508" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-10.795" y1="3.683" x2="-10.795" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-10.795" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-3.683" x2="-10.795" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-11.43" y2="-3.683" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-3.175" x2="-10.795" y2="-3.175" width="0.1524" layer="51"/>
<wire x1="-10.795" y1="-3.175" x2="-10.795" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-3.683" x2="-11.43" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-1.27" x2="-11.176" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-1.27" x2="-11.176" y2="-3.175" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-3.683" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-3.683" x2="10.795" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="5.715" y1="3.683" x2="10.795" y2="3.683" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="11.43" y2="3.683" width="0.1524" layer="51"/>
<wire x1="11.176" y1="3.175" x2="10.795" y2="3.175" width="0.1524" layer="51"/>
<wire x1="10.795" y1="3.175" x2="10.795" y2="2.54" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.54" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.683" x2="11.43" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.43" y1="1.27" x2="11.176" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.176" y1="1.27" x2="11.176" y2="3.175" width="0.1524" layer="51"/>
<wire x1="12.573" y1="5.461" x2="12.573" y2="2.032" width="0.1524" layer="21"/>
<wire x1="12.573" y1="-2.032" x2="12.573" y2="2.032" width="0.1524" layer="51"/>
<wire x1="12.573" y1="-2.032" x2="12.573" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="5.461" x2="-12.573" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-12.573" y1="-2.032" x2="-12.573" y2="2.032" width="0.1524" layer="51"/>
<wire x1="-12.573" y1="-2.032" x2="-12.573" y2="-5.461" width="0.1524" layer="21"/>
<pad name="1" x="-11.2522" y="0" drill="1.3208" diameter="2.54" shape="long" rot="R90"/>
<pad name="2" x="11.2522" y="0" drill="1.3208" diameter="2.54" shape="long" rot="R90"/>
<text x="-12.7" y="-7.62" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.0254" y="-7.62" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.541" y1="-2.032" x2="-10.16" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="1.905" x2="-9.652" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="-2.032" x2="-9.652" y2="-1.905" layer="51"/>
<rectangle x1="-5.715" y1="-3.175" x2="-5.08" y2="-2.032" layer="21"/>
<rectangle x1="-5.715" y1="2.032" x2="-5.08" y2="3.175" layer="21"/>
<rectangle x1="-5.715" y1="-2.032" x2="-5.08" y2="2.032" layer="21"/>
<rectangle x1="10.16" y1="-2.032" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="9.652" y1="-2.032" x2="10.541" y2="-1.905" layer="51"/>
<rectangle x1="9.652" y1="1.905" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="5.08" y1="2.032" x2="5.715" y2="3.175" layer="21"/>
<rectangle x1="5.08" y1="-3.175" x2="5.715" y2="-2.032" layer="21"/>
<rectangle x1="5.08" y1="-2.032" x2="5.715" y2="2.032" layer="21"/>
<rectangle x1="-5.08" y1="2.159" x2="5.08" y2="2.667" layer="21"/>
<rectangle x1="-5.08" y1="-2.667" x2="5.08" y2="-2.159" layer="21"/>
<rectangle x1="5.715" y1="-2.032" x2="9.652" y2="-1.905" layer="21"/>
<rectangle x1="5.715" y1="1.905" x2="9.652" y2="2.032" layer="51"/>
<rectangle x1="-9.652" y1="1.905" x2="-5.715" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="-2.032" x2="-5.715" y2="-1.905" layer="21"/>
<rectangle x1="-10.541" y1="-2.54" x2="-5.715" y2="-2.032" layer="51"/>
<rectangle x1="-10.541" y1="2.032" x2="-5.715" y2="2.54" layer="51"/>
<rectangle x1="5.715" y1="2.032" x2="10.541" y2="2.54" layer="51"/>
<rectangle x1="5.715" y1="-2.54" x2="10.541" y2="-2.032" layer="51"/>
</package>
<package name="SH22,5_SMD_OH">
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="5.08" y2="2.921" width="0.0508" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="3.683" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-10.795" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-3.683" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.683" x2="-11.43" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-3.683" x2="-11.43" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-2.032" x2="-11.176" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-11.176" y1="-3.175" x2="-10.795" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-3.175" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-2.032" x2="-11.43" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.43" y1="-1.27" x2="-11.176" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-11.176" y1="-1.27" x2="-11.176" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-3.683" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-3.683" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="10.795" y2="3.683" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.683" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.683" x2="11.43" y2="3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.683" x2="11.43" y2="2.032" width="0.1524" layer="21"/>
<wire x1="11.176" y1="2.032" x2="11.176" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.175" x2="10.795" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.175" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="5.715" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="11.43" y1="2.032" x2="11.43" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.43" y1="1.27" x2="11.176" y2="1.27" width="0.1524" layer="51"/>
<wire x1="11.176" y1="1.27" x2="11.176" y2="2.032" width="0.1524" layer="51"/>
<smd name="1" x="-8.5" y="0" dx="6" dy="6" layer="1"/>
<smd name="2" x="8.5" y="0" dx="6" dy="6" layer="1"/>
<text x="-12.7" y="-6.096" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="2.413" y="-6.096" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.541" y1="-2.032" x2="-10.16" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="1.905" x2="-9.652" y2="2.032" layer="51"/>
<rectangle x1="-10.541" y1="-2.032" x2="-9.652" y2="-1.905" layer="51"/>
<rectangle x1="-5.715" y1="-3.175" x2="-5.08" y2="-2.032" layer="21"/>
<rectangle x1="-5.715" y1="2.032" x2="-5.08" y2="3.175" layer="21"/>
<rectangle x1="-5.715" y1="-2.032" x2="-5.08" y2="2.032" layer="21"/>
<rectangle x1="10.16" y1="-2.032" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="9.652" y1="-2.032" x2="10.541" y2="-1.905" layer="51"/>
<rectangle x1="9.652" y1="1.905" x2="10.541" y2="2.032" layer="51"/>
<rectangle x1="5.08" y1="2.032" x2="5.715" y2="3.175" layer="21"/>
<rectangle x1="5.08" y1="-3.175" x2="5.715" y2="-2.032" layer="21"/>
<rectangle x1="5.08" y1="-2.032" x2="5.715" y2="2.032" layer="21"/>
<rectangle x1="-5.08" y1="2.159" x2="5.08" y2="2.667" layer="21"/>
<rectangle x1="-5.08" y1="-2.667" x2="5.08" y2="-2.159" layer="21"/>
<rectangle x1="5.715" y1="-2.032" x2="9.652" y2="-1.905" layer="21"/>
<rectangle x1="5.715" y1="1.905" x2="9.652" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="1.905" x2="-5.715" y2="2.032" layer="21"/>
<rectangle x1="-9.652" y1="-2.032" x2="-5.715" y2="-1.905" layer="21"/>
<rectangle x1="-10.541" y1="-2.54" x2="-5.715" y2="-2.032" layer="21"/>
<rectangle x1="-10.541" y1="2.032" x2="-5.715" y2="2.54" layer="21"/>
<rectangle x1="5.715" y1="2.032" x2="10.541" y2="2.54" layer="21"/>
<rectangle x1="5.715" y1="-2.54" x2="10.541" y2="-2.032" layer="21"/>
</package>
<package name="5X20">
<wire x1="-5.207" y1="0" x2="5.207" y2="0" width="0.0508" layer="21"/>
<text x="-4.953" y="0.254" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.9276" y="-2.032" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="FH">
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.794" y="3.683" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-0.508" y="1.905" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="FUSE-S">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="0" width="0.254" layer="94"/>
<text x="-2.794" y="-1.905" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-0.254" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5X20MM" prefix="FH">
<description>&lt;b&gt;Fuse Holder&lt;/b&gt;&lt;p&gt; 22.5 mm</description>
<gates>
<gate name="G$1" symbol="FH" x="0" y="0"/>
</gates>
<devices>
<device name="_SMD" package="SH22,5_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SH22,5A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="WUERTH" value="696103201002" constant="no"/>
</technology>
</technologies>
</device>
<device name="_SMD_OH" package="SH22,5_SMD_OH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_G" package="SH22,5A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="WUERTH" value="696108003002" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5X20_1AT" prefix="F">
<description>1 A, träge, 5x20</description>
<gates>
<gate name="G$1" symbol="FUSE-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="5X20">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="Frames" deviceset="RAHMEN3Q" device=""/>
<part name="IC2" library="Microcontroller" deviceset="AT89S51-*" device="P"/>
<part name="XTAL1" library="Crystal" deviceset="7.3728MHZ" device="/S"/>
<part name="C4" library="C-0805" deviceset="22P" device=""/>
<part name="C3" library="C-0805" deviceset="22P" device=""/>
<part name="SUPPLY2" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY1" library="Supply" deviceset="0V" device=""/>
<part name="C1" library="C-Draht" deviceset="220N/305V/MKP-X2" device=""/>
<part name="R1" library="R-th" deviceset="220R" device="/10"/>
<part name="D3" library="Diode" deviceset="BZX55C5V1" device="/7"/>
<part name="D1" library="Diode" deviceset="1N4007" device=""/>
<part name="D2" library="Diode" deviceset="1N4007" device=""/>
<part name="C2" library="C-Elko" deviceset="470U/63V" device=""/>
<part name="R2" library="R-th" deviceset="470K" device="/15"/>
<part name="SUPPLY3" library="Supply" deviceset="R" device=""/>
<part name="SUPPLY5" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY6" library="Supply" deviceset="0V" device=""/>
<part name="C5" library="C-0805" deviceset="100N" device=""/>
<part name="C6" library="C-Elko" deviceset="10U/63V" device=""/>
<part name="SUPPLY12" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY13" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY14" library="Supply" deviceset="0V" device=""/>
<part name="R5" library="R-0805" deviceset="10K" device=""/>
<part name="Q2" library="Transistor" deviceset="2N3906" device=""/>
<part name="T1" library="Transistor" deviceset="BT136" device=""/>
<part name="R6" library="R-0805" deviceset="10K" device=""/>
<part name="R7" library="R-0805" deviceset="10K" device=""/>
<part name="SUPPLY18" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY20" library="Supply" deviceset="PE" device=""/>
<part name="S1" library="switch" deviceset="24-1POLE36" device=""/>
<part name="S2" library="switch" deviceset="24-1POLE36" device=""/>
<part name="S3" library="switch" deviceset="24-1POLE36" device=""/>
<part name="D4" library="Diode" deviceset="LL4148" device=""/>
<part name="D5" library="Diode" deviceset="LL4148" device=""/>
<part name="D6" library="Diode" deviceset="LL4148" device=""/>
<part name="S4" library="e-schalter" deviceset="TASTER_SCHLIESSER" device=""/>
<part name="S5" library="e-schalter" deviceset="TASTER_SCHLIESSER" device=""/>
<part name="SUPPLY21" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY22" library="Supply" deviceset="0V" device=""/>
<part name="FH1" library="Fuses" deviceset="5X20MM" device=""/>
<part name="F1" library="Fuses" deviceset="5X20_1AT" device=""/>
<part name="S6" library="e-schalter" deviceset="TASTER_SCHLIESSER" device=""/>
<part name="SUPPLY8" library="Supply" deviceset="0V" device=""/>
<part name="D7" library="Diode" deviceset="BZX55C5V1" device="/7"/>
<part name="C7" library="C-Elko" deviceset="470U/63V" device=""/>
<part name="SUPPLY9" library="Supply" deviceset="0V" device=""/>
<part name="SUPPLY19" library="Supply" deviceset="0V" device=""/>
<part name="Q1" library="Transistor" deviceset="2N3904" device=""/>
<part name="R3" library="R-0805" deviceset="220R" device=""/>
<part name="R8" library="R-th" deviceset="470K" device="/10"/>
<part name="D8" library="Diode" deviceset="BZX55C5V1" device="/7"/>
<part name="SUPPLY24" library="Supply" deviceset="0V" device=""/>
<part name="R9" library="R-0805" deviceset="5K6" device=""/>
<part name="SUPPLY4" library="Supply" deviceset="+5V" device=""/>
<part name="SUPPLY11" library="Supply" deviceset="+5V" device=""/>
<part name="SUPPLY15" library="Supply" deviceset="+5V" device=""/>
<part name="SUPPLY17" library="Supply" deviceset="+5V" device=""/>
<part name="SUPPLY16" library="Supply" deviceset="+5V" device=""/>
<part name="SUPPLY10" library="Supply" deviceset="+5V" device=""/>
<part name="SUPPLY23" library="Supply" deviceset="-5V" device=""/>
<part name="SUPPLY25" library="Supply" deviceset="-5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<circle x="345.44" y="200.66" radius="7.62" width="0.1524" layer="94"/>
<circle x="345.44" y="204.47" radius="1.04726875" width="0.1524" layer="94"/>
<circle x="345.44" y="196.85" radius="1.04726875" width="0.1524" layer="94"/>
<wire x1="342.9" y1="207.772" x2="342.9" y2="205.74" width="0.1524" layer="94"/>
<wire x1="342.9" y1="205.74" x2="339.852" y2="205.74" width="0.1524" layer="94"/>
<wire x1="347.98" y1="207.772" x2="347.98" y2="205.74" width="0.1524" layer="94"/>
<wire x1="347.98" y1="205.74" x2="351.028" y2="205.74" width="0.1524" layer="94"/>
<wire x1="342.9" y1="193.548" x2="342.9" y2="195.58" width="0.1524" layer="94"/>
<wire x1="342.9" y1="195.58" x2="339.852" y2="195.58" width="0.1524" layer="94"/>
<wire x1="347.98" y1="193.548" x2="347.98" y2="195.58" width="0.1524" layer="94"/>
<wire x1="347.98" y1="195.58" x2="351.028" y2="195.58" width="0.1524" layer="94"/>
<wire x1="337.82" y1="201.168" x2="339.598" y2="201.168" width="0.1524" layer="94"/>
<wire x1="339.598" y1="201.168" x2="339.598" y2="200.152" width="0.1524" layer="94"/>
<wire x1="339.598" y1="200.152" x2="337.82" y2="200.152" width="0.1524" layer="94"/>
<wire x1="353.06" y1="201.168" x2="351.282" y2="201.168" width="0.1524" layer="94"/>
<wire x1="351.282" y1="201.168" x2="351.282" y2="200.152" width="0.1524" layer="94"/>
<wire x1="351.282" y1="200.152" x2="353.06" y2="200.152" width="0.1524" layer="94"/>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="209.55" y="3.81"/>
<instance part="IC2" gate="P" x="248.92" y="170.18"/>
<instance part="XTAL1" gate="G$1" x="157.48" y="193.04" rot="R90"/>
<instance part="C4" gate="G$1" x="144.78" y="187.96" rot="R270"/>
<instance part="C3" gate="G$1" x="144.78" y="195.58" rot="R270"/>
<instance part="SUPPLY2" gate="0V" x="134.62" y="182.88"/>
<instance part="SUPPLY1" gate="0V" x="127" y="182.88"/>
<instance part="C1" gate="G$1" x="50.8" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="49.784" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="42.164" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="30.48" y="228.6"/>
<instance part="D3" gate="U" x="91.44" y="213.36" rot="R90"/>
<instance part="D1" gate="U" x="71.12" y="213.36" rot="R90"/>
<instance part="D2" gate="U" x="81.28" y="228.6"/>
<instance part="C2" gate="G$1" x="101.6" y="215.9"/>
<instance part="R2" gate="G$1" x="50.8" y="220.98"/>
<instance part="SUPPLY3" gate="G$1" x="20.32" y="266.7"/>
<instance part="SUPPLY5" gate="0V" x="91.44" y="205.74"/>
<instance part="SUPPLY6" gate="0V" x="101.6" y="205.74"/>
<instance part="C5" gate="G$1" x="76.2" y="134.62"/>
<instance part="C6" gate="G$1" x="208.28" y="213.36" rot="MR90"/>
<instance part="SUPPLY12" gate="0V" x="274.32" y="134.62"/>
<instance part="SUPPLY13" gate="0V" x="76.2" y="121.92"/>
<instance part="SUPPLY14" gate="0V" x="200.66" y="200.66"/>
<instance part="R5" gate="G$1" x="276.86" y="149.86"/>
<instance part="Q2" gate="U" x="317.5" y="195.58"/>
<instance part="T1" gate="-1" x="345.44" y="172.72"/>
<instance part="R6" gate="G$1" x="302.26" y="195.58" smashed="yes">
<attribute name="NAME" x="298.45" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="298.45" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="320.04" y="182.88" rot="R90"/>
<instance part="SUPPLY18" gate="0V" x="345.44" y="165.1"/>
<instance part="SUPPLY20" gate="PE" x="363.22" y="182.88"/>
<instance part="S1" gate="G$1" x="132.08" y="142.24" rot="MR270"/>
<instance part="S2" gate="G$1" x="132.08" y="104.14" rot="MR270"/>
<instance part="S3" gate="G$1" x="132.08" y="66.04" rot="MR270"/>
<instance part="D4" gate="U" x="109.22" y="152.4" rot="R180"/>
<instance part="D5" gate="U" x="109.22" y="114.3" rot="R180"/>
<instance part="D6" gate="U" x="109.22" y="76.2" rot="R180"/>
<instance part="S4" gate="G$1" x="193.04" y="116.84"/>
<instance part="S5" gate="G$1" x="203.2" y="116.84"/>
<instance part="SUPPLY21" gate="0V" x="193.04" y="106.68"/>
<instance part="SUPPLY22" gate="0V" x="203.2" y="106.68"/>
<instance part="FH1" gate="G$1" x="20.32" y="254" rot="R90"/>
<instance part="F1" gate="G$1" x="20.32" y="254" rot="R90"/>
<instance part="S6" gate="G$1" x="213.36" y="116.84"/>
<instance part="SUPPLY8" gate="0V" x="213.36" y="106.68"/>
<instance part="D7" gate="U" x="91.44" y="180.34" smashed="yes" rot="R270">
<attribute name="NAME" x="93.345" y="182.118" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="88.138" y="181.229" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C7" gate="G$1" x="101.6" y="180.34" rot="MR180"/>
<instance part="SUPPLY9" gate="0V" x="101.6" y="172.72"/>
<instance part="SUPPLY19" gate="0V" x="91.44" y="172.72"/>
<instance part="Q1" gate="U" x="332.74" y="154.94"/>
<instance part="R3" gate="G$1" x="335.28" y="165.1" rot="R90"/>
<instance part="R8" gate="G$1" x="20.32" y="134.62" rot="R90"/>
<instance part="D8" gate="U" x="292.1" y="86.36" rot="R90"/>
<instance part="SUPPLY24" gate="0V" x="292.1" y="76.2"/>
<instance part="R9" gate="G$1" x="287.02" y="205.74" rot="R90"/>
<instance part="SUPPLY4" gate="+5V" x="101.6" y="236.22"/>
<instance part="SUPPLY11" gate="+5V" x="274.32" y="223.52"/>
<instance part="SUPPLY15" gate="+5V" x="287.02" y="223.52"/>
<instance part="SUPPLY17" gate="+5V" x="320.04" y="223.52"/>
<instance part="SUPPLY16" gate="+5V" x="287.02" y="154.94"/>
<instance part="SUPPLY10" gate="+5V" x="76.2" y="144.78"/>
<instance part="SUPPLY23" gate="-5V" x="335.28" y="144.78"/>
<instance part="SUPPLY25" gate="-5V" x="101.6" y="195.58" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="IC2" gate="P" pin="XTAL1"/>
<pinref part="XTAL1" gate="G$1" pin="2"/>
<wire x1="228.6" y1="195.58" x2="157.48" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="157.48" y1="195.58" x2="147.32" y2="195.58" width="0.1524" layer="91"/>
<junction x="157.48" y="195.58"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="IC2" gate="P" pin="XTAL2"/>
<wire x1="147.32" y1="187.96" x2="157.48" y2="187.96" width="0.1524" layer="91"/>
<pinref part="XTAL1" gate="G$1" pin="1"/>
<wire x1="157.48" y1="187.96" x2="228.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="157.48" y1="190.5" x2="157.48" y2="187.96" width="0.1524" layer="91"/>
<junction x="157.48" y="187.96"/>
</segment>
</net>
<net name="0V" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="0V" pin="0V"/>
<wire x1="139.7" y1="195.58" x2="127" y2="195.58" width="0.1524" layer="91"/>
<wire x1="127" y1="195.58" x2="127" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="0V" pin="0V"/>
<wire x1="139.7" y1="187.96" x2="134.62" y2="187.96" width="0.1524" layer="91"/>
<wire x1="134.62" y1="187.96" x2="134.62" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D3" gate="U" pin="A"/>
<pinref part="SUPPLY5" gate="0V" pin="0V"/>
<wire x1="91.44" y1="208.28" x2="91.44" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="-"/>
<pinref part="SUPPLY6" gate="0V" pin="0V"/>
<wire x1="101.6" y1="210.82" x2="101.6" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="0V" pin="0V"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="76.2" y1="124.46" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="P" pin="GND"/>
<pinref part="SUPPLY12" gate="0V" pin="0V"/>
<wire x1="269.24" y1="139.7" x2="274.32" y2="139.7" width="0.1524" layer="91"/>
<wire x1="274.32" y1="139.7" x2="274.32" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="0V" pin="0V"/>
<wire x1="203.2" y1="213.36" x2="200.66" y2="213.36" width="0.1524" layer="91"/>
<wire x1="200.66" y1="213.36" x2="200.66" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="T1" gate="-1" pin="A1"/>
<pinref part="SUPPLY18" gate="0V" pin="0V"/>
<wire x1="345.44" y1="170.18" x2="345.44" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S5" gate="G$1" pin="14"/>
<pinref part="SUPPLY22" gate="0V" pin="0V"/>
<wire x1="203.2" y1="111.76" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S4" gate="G$1" pin="14"/>
<pinref part="SUPPLY21" gate="0V" pin="0V"/>
<wire x1="193.04" y1="111.76" x2="193.04" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S6" gate="G$1" pin="14"/>
<pinref part="SUPPLY8" gate="0V" pin="0V"/>
<wire x1="213.36" y1="111.76" x2="213.36" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D7" gate="U" pin="C"/>
<pinref part="SUPPLY19" gate="0V" pin="0V"/>
<wire x1="91.44" y1="177.8" x2="91.44" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="+"/>
<pinref part="SUPPLY9" gate="0V" pin="0V"/>
<wire x1="101.6" y1="177.8" x2="101.6" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D8" gate="U" pin="A"/>
<pinref part="SUPPLY24" gate="0V" pin="0V"/>
<wire x1="292.1" y1="83.82" x2="292.1" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="35.56" y1="228.6" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="40.64" y1="228.6" x2="48.26" y2="228.6" width="0.1524" layer="91"/>
<wire x1="45.72" y1="220.98" x2="40.64" y2="220.98" width="0.1524" layer="91"/>
<wire x1="40.64" y1="220.98" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<junction x="40.64" y="228.6"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="D2" gate="U" pin="A"/>
<wire x1="78.74" y1="228.6" x2="71.12" y2="228.6" width="0.1524" layer="91"/>
<pinref part="D1" gate="U" pin="K"/>
<wire x1="71.12" y1="228.6" x2="55.88" y2="228.6" width="0.1524" layer="91"/>
<wire x1="71.12" y1="215.9" x2="71.12" y2="220.98" width="0.1524" layer="91"/>
<junction x="71.12" y="228.6"/>
<wire x1="71.12" y1="220.98" x2="71.12" y2="228.6" width="0.1524" layer="91"/>
<wire x1="55.88" y1="220.98" x2="71.12" y2="220.98" width="0.1524" layer="91"/>
<junction x="71.12" y="220.98"/>
</segment>
</net>
<net name="R" class="0">
<segment>
<pinref part="SUPPLY3" gate="G$1" pin="R"/>
<pinref part="FH1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="264.16" x2="20.32" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC2" gate="P" pin="RST"/>
<wire x1="228.6" y1="200.66" x2="215.9" y2="200.66" width="0.1524" layer="91"/>
<wire x1="215.9" y1="200.66" x2="215.9" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="+"/>
<wire x1="210.82" y1="213.36" x2="215.9" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="IC2" gate="P" pin="!EA!/VPP"/>
<wire x1="271.78" y1="149.86" x2="269.24" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="Q2" gate="U" pin="B"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="314.96" y1="195.58" x2="307.34" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="IC2" gate="P" pin="P0.0_AD0"/>
<wire x1="297.18" y1="195.58" x2="287.02" y2="195.58" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="287.02" y1="195.58" x2="269.24" y2="195.58" width="0.1524" layer="91"/>
<wire x1="287.02" y1="200.66" x2="287.02" y2="195.58" width="0.1524" layer="91"/>
<junction x="287.02" y="195.58"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Q2" gate="U" pin="C"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="320.04" y1="190.5" x2="320.04" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="Q1" gate="U" pin="B"/>
<wire x1="330.2" y1="154.94" x2="320.04" y2="154.94" width="0.1524" layer="91"/>
<wire x1="320.04" y1="154.94" x2="320.04" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<pinref part="SUPPLY20" gate="PE" pin="PE"/>
<wire x1="353.06" y1="200.66" x2="363.22" y2="200.66" width="0.1524" layer="91"/>
<wire x1="363.22" y1="200.66" x2="363.22" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="T1" gate="-1" pin="A2"/>
<wire x1="345.44" y1="195.58" x2="345.44" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="139.7" y1="76.2" x2="149.86" y2="76.2" width="0.1524" layer="91"/>
<wire x1="149.86" y1="76.2" x2="149.86" y2="114.3" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.0"/>
<wire x1="149.86" y1="114.3" x2="149.86" y2="152.4" width="0.1524" layer="91"/>
<wire x1="149.86" y1="152.4" x2="149.86" y2="182.88" width="0.1524" layer="91"/>
<wire x1="149.86" y1="182.88" x2="228.6" y2="182.88" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="139.7" y1="152.4" x2="149.86" y2="152.4" width="0.1524" layer="91"/>
<junction x="149.86" y="152.4"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="139.7" y1="114.3" x2="149.86" y2="114.3" width="0.1524" layer="91"/>
<junction x="149.86" y="114.3"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="139.7" y1="149.86" x2="152.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="152.4" y1="149.86" x2="152.4" y2="180.34" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.1"/>
<wire x1="152.4" y1="180.34" x2="228.6" y2="180.34" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="2"/>
<wire x1="139.7" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<junction x="152.4" y="149.86"/>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="152.4" y1="111.76" x2="152.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="139.7" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<junction x="152.4" y="111.76"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="3"/>
<wire x1="139.7" y1="71.12" x2="154.94" y2="71.12" width="0.1524" layer="91"/>
<wire x1="154.94" y1="71.12" x2="154.94" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.2"/>
<wire x1="154.94" y1="109.22" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<wire x1="154.94" y1="147.32" x2="154.94" y2="177.8" width="0.1524" layer="91"/>
<wire x1="154.94" y1="177.8" x2="228.6" y2="177.8" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="139.7" y1="147.32" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<junction x="154.94" y="147.32"/>
<pinref part="S2" gate="G$1" pin="3"/>
<wire x1="139.7" y1="109.22" x2="154.94" y2="109.22" width="0.1524" layer="91"/>
<junction x="154.94" y="109.22"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="4"/>
<wire x1="139.7" y1="68.58" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<wire x1="157.48" y1="68.58" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.3"/>
<wire x1="157.48" y1="106.68" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<wire x1="157.48" y1="144.78" x2="157.48" y2="175.26" width="0.1524" layer="91"/>
<wire x1="157.48" y1="175.26" x2="228.6" y2="175.26" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="139.7" y1="144.78" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<junction x="157.48" y="144.78"/>
<pinref part="S2" gate="G$1" pin="4"/>
<wire x1="139.7" y1="106.68" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<junction x="157.48" y="106.68"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="5"/>
<wire x1="139.7" y1="66.04" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<wire x1="160.02" y1="66.04" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.4"/>
<wire x1="160.02" y1="104.14" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<wire x1="160.02" y1="142.24" x2="160.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="160.02" y1="172.72" x2="228.6" y2="172.72" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="5"/>
<wire x1="139.7" y1="142.24" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<junction x="160.02" y="142.24"/>
<pinref part="S2" gate="G$1" pin="5"/>
<wire x1="139.7" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<junction x="160.02" y="104.14"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="6"/>
<wire x1="139.7" y1="63.5" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="162.56" y1="63.5" x2="162.56" y2="101.6" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.5_MOSI"/>
<wire x1="162.56" y1="101.6" x2="162.56" y2="139.7" width="0.1524" layer="91"/>
<wire x1="162.56" y1="139.7" x2="162.56" y2="170.18" width="0.1524" layer="91"/>
<wire x1="162.56" y1="170.18" x2="228.6" y2="170.18" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="6"/>
<wire x1="139.7" y1="139.7" x2="162.56" y2="139.7" width="0.1524" layer="91"/>
<junction x="162.56" y="139.7"/>
<pinref part="S2" gate="G$1" pin="6"/>
<wire x1="139.7" y1="101.6" x2="162.56" y2="101.6" width="0.1524" layer="91"/>
<junction x="162.56" y="101.6"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="7"/>
<wire x1="139.7" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="99.06" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.6_MISO"/>
<wire x1="165.1" y1="99.06" x2="165.1" y2="137.16" width="0.1524" layer="91"/>
<wire x1="165.1" y1="137.16" x2="165.1" y2="167.64" width="0.1524" layer="91"/>
<wire x1="165.1" y1="167.64" x2="228.6" y2="167.64" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="7"/>
<wire x1="139.7" y1="137.16" x2="165.1" y2="137.16" width="0.1524" layer="91"/>
<junction x="165.1" y="137.16"/>
<pinref part="S2" gate="G$1" pin="7"/>
<wire x1="139.7" y1="99.06" x2="165.1" y2="99.06" width="0.1524" layer="91"/>
<junction x="165.1" y="99.06"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="8"/>
<wire x1="139.7" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="58.42" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P1.7_SCK"/>
<wire x1="167.64" y1="96.52" x2="167.64" y2="134.62" width="0.1524" layer="91"/>
<wire x1="167.64" y1="134.62" x2="167.64" y2="165.1" width="0.1524" layer="91"/>
<wire x1="167.64" y1="165.1" x2="228.6" y2="165.1" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="8"/>
<wire x1="139.7" y1="134.62" x2="167.64" y2="134.62" width="0.1524" layer="91"/>
<junction x="167.64" y="134.62"/>
<pinref part="S2" gate="G$1" pin="8"/>
<wire x1="139.7" y1="96.52" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
<junction x="167.64" y="96.52"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="9"/>
<wire x1="139.7" y1="55.88" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="170.18" y1="55.88" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P3.0/RXD"/>
<wire x1="170.18" y1="93.98" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
<wire x1="170.18" y1="132.08" x2="170.18" y2="160.02" width="0.1524" layer="91"/>
<wire x1="170.18" y1="160.02" x2="228.6" y2="160.02" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="9"/>
<wire x1="139.7" y1="132.08" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
<junction x="170.18" y="132.08"/>
<pinref part="S2" gate="G$1" pin="9"/>
<wire x1="139.7" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<junction x="170.18" y="93.98"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="10"/>
<wire x1="139.7" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="172.72" y1="53.34" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P3.1/TXD"/>
<wire x1="172.72" y1="91.44" x2="172.72" y2="129.54" width="0.1524" layer="91"/>
<wire x1="172.72" y1="129.54" x2="172.72" y2="157.48" width="0.1524" layer="91"/>
<wire x1="172.72" y1="157.48" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="10"/>
<wire x1="139.7" y1="129.54" x2="172.72" y2="129.54" width="0.1524" layer="91"/>
<junction x="172.72" y="129.54"/>
<pinref part="S2" gate="G$1" pin="10"/>
<wire x1="139.7" y1="91.44" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<junction x="172.72" y="91.44"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="S"/>
<pinref part="D4" gate="U" pin="A"/>
<wire x1="124.46" y1="152.4" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="S"/>
<wire x1="124.46" y1="114.3" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
<pinref part="D5" gate="U" pin="A"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="S"/>
<pinref part="D6" gate="U" pin="A"/>
<wire x1="124.46" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D6" gate="U" pin="K"/>
<wire x1="106.68" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="104.14" y2="48.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="48.26" x2="177.8" y2="48.26" width="0.1524" layer="91"/>
<wire x1="177.8" y1="48.26" x2="177.8" y2="154.94" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P3.2/!INT0"/>
<wire x1="177.8" y1="154.94" x2="228.6" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D5" gate="U" pin="K"/>
<wire x1="106.68" y1="114.3" x2="101.6" y2="114.3" width="0.1524" layer="91"/>
<wire x1="101.6" y1="114.3" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<wire x1="101.6" y1="45.72" x2="180.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="180.34" y1="45.72" x2="180.34" y2="152.4" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P3.3/!INT1"/>
<wire x1="180.34" y1="152.4" x2="228.6" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D4" gate="U" pin="K"/>
<wire x1="106.68" y1="152.4" x2="99.06" y2="152.4" width="0.1524" layer="91"/>
<wire x1="99.06" y1="152.4" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<wire x1="99.06" y1="43.18" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
<wire x1="182.88" y1="43.18" x2="182.88" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P3.4/T0"/>
<wire x1="182.88" y1="149.86" x2="228.6" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="IC2" gate="P" pin="P3.5/T1"/>
<pinref part="S4" gate="G$1" pin="13"/>
<wire x1="228.6" y1="147.32" x2="193.04" y2="147.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="147.32" x2="193.04" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="IC2" gate="P" pin="P3.6/!WR"/>
<pinref part="S5" gate="G$1" pin="13"/>
<wire x1="228.6" y1="144.78" x2="203.2" y2="144.78" width="0.1524" layer="91"/>
<wire x1="203.2" y1="144.78" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="228.6" x2="25.4" y2="228.6" width="0.1524" layer="91"/>
<pinref part="FH1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="246.38" x2="20.32" y2="243.84" width="0.1524" layer="91"/>
<wire x1="20.32" y1="243.84" x2="20.32" y2="228.6" width="0.1524" layer="91"/>
<wire x1="20.32" y1="243.84" x2="345.44" y2="243.84" width="0.1524" layer="91"/>
<wire x1="345.44" y1="243.84" x2="345.44" y2="205.74" width="0.1524" layer="91"/>
<junction x="20.32" y="243.84"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="20.32" y1="228.6" x2="20.32" y2="139.7" width="0.1524" layer="91"/>
<junction x="20.32" y="228.6"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="S6" gate="G$1" pin="13"/>
<wire x1="213.36" y1="121.92" x2="213.36" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P3.7/!RD"/>
<wire x1="213.36" y1="142.24" x2="228.6" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="Q1" gate="U" pin="C"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="T1" gate="-1" pin="G"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="340.36" y1="172.72" x2="335.28" y2="172.72" width="0.1524" layer="91"/>
<wire x1="335.28" y1="172.72" x2="335.28" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="20.32" y1="129.54" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="35.56" x2="198.12" y2="35.56" width="0.1524" layer="91"/>
<wire x1="198.12" y1="35.56" x2="198.12" y2="99.06" width="0.1524" layer="91"/>
<wire x1="198.12" y1="99.06" x2="292.1" y2="99.06" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="P0.1_AD1"/>
<wire x1="292.1" y1="99.06" x2="292.1" y2="193.04" width="0.1524" layer="91"/>
<wire x1="292.1" y1="193.04" x2="269.24" y2="193.04" width="0.1524" layer="91"/>
<pinref part="D8" gate="U" pin="C"/>
<wire x1="292.1" y1="99.06" x2="292.1" y2="88.9" width="0.1524" layer="91"/>
<junction x="292.1" y="99.06"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="D2" gate="U" pin="K"/>
<pinref part="D3" gate="U" pin="C"/>
<wire x1="83.82" y1="228.6" x2="91.44" y2="228.6" width="0.1524" layer="91"/>
<wire x1="91.44" y1="228.6" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="91.44" y1="228.6" x2="101.6" y2="228.6" width="0.1524" layer="91"/>
<wire x1="101.6" y1="228.6" x2="101.6" y2="218.44" width="0.1524" layer="91"/>
<junction x="91.44" y="228.6"/>
<wire x1="101.6" y1="228.6" x2="101.6" y2="233.68" width="0.1524" layer="91"/>
<junction x="101.6" y="228.6"/>
<pinref part="SUPPLY4" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="281.94" y1="149.86" x2="287.02" y2="149.86" width="0.1524" layer="91"/>
<wire x1="287.02" y1="149.86" x2="287.02" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="76.2" y1="142.24" x2="76.2" y2="137.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="+5V" pin="+5V"/>
</segment>
<segment>
<wire x1="274.32" y1="220.98" x2="274.32" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC2" gate="P" pin="VCC"/>
<wire x1="274.32" y1="200.66" x2="269.24" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="+5V" pin="+5V"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="287.02" y1="220.98" x2="287.02" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="U" pin="E"/>
<wire x1="320.04" y1="220.98" x2="320.04" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY17" gate="+5V" pin="+5V"/>
</segment>
</net>
<net name="-5V" class="0">
<segment>
<pinref part="Q1" gate="U" pin="E"/>
<wire x1="335.28" y1="149.86" x2="335.28" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="-5V" pin="-5V"/>
</segment>
<segment>
<pinref part="D1" gate="U" pin="A"/>
<wire x1="71.12" y1="210.82" x2="71.12" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="-"/>
<wire x1="71.12" y1="187.96" x2="91.44" y2="187.96" width="0.1524" layer="91"/>
<wire x1="91.44" y1="187.96" x2="101.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="101.6" y1="187.96" x2="101.6" y2="185.42" width="0.1524" layer="91"/>
<pinref part="D7" gate="U" pin="A"/>
<wire x1="91.44" y1="182.88" x2="91.44" y2="187.96" width="0.1524" layer="91"/>
<junction x="91.44" y="187.96"/>
<wire x1="101.6" y1="193.04" x2="101.6" y2="187.96" width="0.1524" layer="91"/>
<junction x="101.6" y="187.96"/>
<pinref part="SUPPLY25" gate="-5V" pin="-5V"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
